// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main2.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 17:12:34 by rduclos           #+#    #+#             //
//   Updated: 2015/01/19 15:49:44 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <curses.h>
#include <locale.h>
#include <iostream>
#include <ctime>

#include "Player.hpp"
#include "EntityManager.hpp"
#include "Screen.hpp"
#include "EntityManager.hpp"
#include "Utils.hpp"

#include <unistd.h>

#define KEY_ESC 27

bool	getTuch(bool quitter, Player & player, float time, EntityManager& entity)
{
	int c = Screen::Instance().getChar();
	time *= 10000;

	switch (c)
	{
	case KEY_DOWN:
		player.makeMove(.3f, 0, 1);
		break;
	case KEY_UP:
		player.makeMove(.3f, 0, -1);
		break;
	case KEY_RIGHT:
		player.makeMove(.3f, 1, 0);
		break;
	case KEY_LEFT:
		player.makeMove(.3f, -1, 0);
		break;
	case 32:
		player.shoot(entity);
	default:
		break;
	}
	return (quitter);
}

void	display(float time, EntityManager& entitys)
{
	static float cumtime = 0.f;

	cumtime += time;
	if (1.f / cumtime <= 60.f)
	{
		Screen::Instance().autoResize();
//		Screen::Instance().clear();
		entitys.drawEverything(Screen::Instance());
		Screen::Instance().drawText(std::to_string(entitys.getPlayer().Score()), 20, 10);
		Screen::Instance().drawText(std::to_string(1.f / time), 0, 0);
//		screen.drawText(std::to_string(frames), 0, 1);
		Screen::Instance().refresh();
	}
}

int		main(void)
{
	bool			quitter = false;
	EntityManager	entitys;
	float			times;

	srand(time(NULL));
	entitys.setPlayerRotSpeed(3.f, 7.f);
	Screen::Instance().init();
	Screen::Instance().autoResize();

	while (!quitter)
	{
		times = Utils::deltaTime();
		quitter = getTuch(quitter, entitys.getPlayer(), times, entitys);
		entitys.moveEverything(times);
		entitys.collideEverything();
		entitys.setPlayerRotate((float)times);
		entitys.moveEverything(times);
		entitys.popWave();
		display(times, entitys);
	}
	while (2) {}
	echo();
	endwin();
	return 0;
}
