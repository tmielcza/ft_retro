#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/01/05 11:49:03 by rduclos           #+#    #+#              #
#    Updated: 2015/01/16 11:55:09 by tmielcza         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=ft_retro
SRC=Screen.cpp VectorUnit.cpp Line.cpp Player.cpp ACharacter.cpp \
	EntityManager.cpp Missile.cpp Enemy.cpp main2.cpp Utils.cpp
OBJ=$(SRC:.cpp=.o)
CPPFLAGS=-Wall -Wextra -Werror -g
CC=/usr/bin/g++

all: $(NAME)

%.o:%.cpp
	$(CC) $(CPPFLAGS) -c -o $@ $<

$(NAME): $(OBJ)
	$(CC) $(CPPFLAGS) -o $(NAME) $(OBJ) -lncurses

clean:
	rm -rf $(OBJ) ./*~ ./#*#

fclean: clean
	rm -f $(NAME)

re: fclean all
