// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 00:39:33 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 17:30:00 by rduclos          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Enemy.hpp"

Enemy::Enemy(e_shape shape, int size) :
	VectorUnit(shape, size)
{
	this->_armormax = size;
	this->_damage = 1;
	this->_type = "Enemy";
	this->_armor = size;
}

Enemy::Enemy(Enemy const & copy)
{
	*this = copy;
}

Enemy::Enemy(void) :
	VectorUnit(__triangle, 1)
{
	this->_x = 10;
	this->_y = 10;
	this->_armormax = 4;
	this->_damage = 1;
	this->_type = "Enemy";
	this->_armor = this->_armormax;
}

Enemy	&			Enemy::operator=(const Enemy& rhs)
{
	this->_x = rhs._x;
	this->_y = rhs._y;
	this->_armormax = rhs._armormax;
	this->_damage = rhs._damage;
	this->_type = rhs._type;
	this->_armor = rhs._armor;
	return (*this);
}

Enemy::~Enemy(void)
{

}

void				Enemy::die(void)
{
	
}
