// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   VectorUnit.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 18:04:04 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/14 16:14:16 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include <cmath>
#include "VectorUnit.hpp"

VectorUnit::VectorUnit(void)
{
}

VectorUnit::VectorUnit(const VectorUnit& src)
{
	*this = src;
}

VectorUnit::~VectorUnit(void)
{
	delete [] this->_lines;
}

VectorUnit::VectorUnit(Line lines[], const int nb)
{
	this->_nblines = nb;
	this->_lines = new Line[nb];
	for (int i = 0; i < nb; i++)
	{
		this->_lines[i] = lines[i];
	}
}

VectorUnit::VectorUnit(e_shape shape, int size)
{
	Line*	(VectorUnit::*f[])(int) = {
		&VectorUnit::UnitSquare,
		&VectorUnit::UnitTriangle,
		&VectorUnit::UnitLine
	};
	int lines[] = {4, 3, 1};

	this->_lines = (this->*f[shape])(size);
	this->_nblines = lines[shape];
	
	this->_speed = 1;
}

Line*			VectorUnit::UnitSquare(int size)
{
	Line*	lines = new Line[4];

	lines[0] = Line(-size, -size, size, -size);
	lines[1] = Line(size, -size, size, size);
	lines[2] = Line(size, size, -size, size);
	lines[3] = Line(-size, size, -size, -size);
	return (lines);
}

Line*			VectorUnit::UnitTriangle(int size)
{
	Line*	lines = new Line[3];

	lines[0] = Line(-size * 2, 0, size, size);
	lines[1] = Line(size, size, size, -size);
	lines[2] = Line(size, -size, -size * 2, 0);
	return (lines);
}

Line*			VectorUnit::UnitLine(int size)
{
	Line*	lines = new Line[1];

	lines[0] = Line(-size, 0, size, 0);
	return (lines);
}

void			VectorUnit::setPos(float x, float y)
{
	this->_x = x;
	this->_y = y;
}

void			VectorUnit::setRot(float rs)
{
	this->_rotspeed = rs;
}

void		VectorUnit::setMove(float vecx, float vecy)
{
	this->_vecx = vecx;
	this->_vecy = vecy;
}

void		VectorUnit::setSpeed(float speed)
{
	this->_speed = speed;
}

VectorUnit&		VectorUnit::operator=(const VectorUnit& rhs)
{
	this->_nblines = rhs._nblines;
	this->_lines = rhs._lines;
	return (*this);
}

void			VectorUnit::display(const Screen& screen) const
{
	for (int i = 0; i < this->_nblines; i++)
	{
		Line	line(this->_lines[i]);

		line = line + Line(this->_x, this->_y, this->_x, this->_y);
		screen.drawLine(line);
	}
}

void			VectorUnit::move(float time)
{
	float	coef = time * this->_speed;

	this->_x += coef * this->_vecx;
	this->_y += coef * this->_vecy;
}

void			VectorUnit::rotate(float time)
{
	float	rot = time * this->_rotspeed;
	
	this->makeRotate(rot);
}

void			VectorUnit::makeRotate(float rot)
{
	for (int i = 0; i < this->_nblines; i++)
	{
		Line*	line(this->_lines + i);

		float xi2 = line->xa() * std::cos(rot) - line->ya() * std::sin(rot);
		float yi2 = line->xa() * std::sin(rot) + line->ya() * std::cos(rot);
		float xf2 = line->xb() * std::cos(rot) - line->yb() * std::sin(rot);
		float yf2 = line->xb() * std::sin(rot) + line->yb() * std::cos(rot);

		Line tmp = Line(xi2, yi2, xf2, yf2);

		this->_lines[i] = tmp;
	}
}

void			VectorUnit::rotateAll(float rot)
{
	float vecx = this->_vecx * std::cos(rot) - this->_vecy * std::sin(rot);
	float vecy = this->_vecx * std::sin(rot) + this->_vecy * std::cos(rot);

	this->_vecx = vecx;
	this->_vecy = vecy;

	this->makeRotate(rot);
}

void		VectorUnit::multSize(float s)
{
	for (int i = 0; i < this->_nblines; i++)
	{
		this->_lines[i] = this->_lines[i] * s;
	}
}

void		VectorUnit::makeMove(float time, float vecx, float vecy)
{
	float	coef = time * this->_speed;

	this->_x += vecx * coef;
	this->_y += vecy * coef;
}

Line*		VectorUnit::getLines(void) const
{
	return (this->_lines);
}

int			VectorUnit::getLineNb(void) const
{
	return (this->_nblines);
}

int			VectorUnit::getX(void) const
{
	return (this->_x);
}

int			VectorUnit::getY(void) const
{
	return (this->_y);
}
