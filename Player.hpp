// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 20:34:13 by rduclos           #+#    #+#             //
//   Updated: 2015/01/12 03:01:52 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef PLAYER_HPP
# define PLAYER_HPP

#include "ACharacter.hpp"
#include "VectorUnit.hpp"
//#include "EntityManager.hpp"

class EntityManager;
class Player : public ACharacter, public VectorUnit
{

	public:

	Player(float x, float y, int armor, int damage);
	Player(Player const & copy);
	Player(void);
	~Player(void);

	Player&			operator=(Player const & rhs);
	void			die(void);
	void			shoot(EntityManager& entity);
	void			makeMove(float time, float vecx, float vecy);

	int				Score(void) const;
	void			Score(int score);

protected:
	int				_score;
};

#endif
