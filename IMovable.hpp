// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IMovable.hpp                                       :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 21:25:46 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/11 13:56:31 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IMOVABLE_HPP
# define IMOVABLE_HPP

class IMovable
{
public:
	virtual ~IMovable() {}
	virtual void	move(float time) = 0;
//	virtual void	makeMove()
};

#endif // IMOVABLE_HPP
