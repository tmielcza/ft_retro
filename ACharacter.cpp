// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ACharacter.cpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 00:19:57 by rduclos           #+#    #+#             //
//   Updated: 2015/01/12 01:23:12 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "ACharacter.hpp"

ACharacter::ACharacter(std::string type, int armormax, int damage)
{
	this->_type = type;
	this->_armor = armormax;
	this->_armormax = armormax;
	this->_damage = damage;
}

ACharacter::ACharacter(ACharacter const & copy)
{
	*this = copy;
}

ACharacter::ACharacter(void)
{
	this->_type = "Missile";
	this->_armor = 1;
	this->_armormax = 1;
	this->_damage = 1;
}

ACharacter::~ACharacter(void)
{
	
}

ACharacter	&	ACharacter::operator=(ACharacter const & ass)
{
	this->_type = ass._type;
	this->_armor = ass._armor;
	this->_armormax = ass._armormax;
	return (*this);
}

void			ACharacter::die(void) const
{
}

int				ACharacter::getArmor(void) const
{
	return (this->_armor);
}

std::string		ACharacter::getType(void) const
{
	return (this->_type);
}

int				ACharacter::getDamage(void) const
{
	return (this->_damage);
}

void			ACharacter::setArmor(int damage)
{
	this->_armor = this->_armor - damage;
	if (this->_armor <= 0)
	{
		_armor = 0;
		this->die();
	}
}

void		ACharacter::colDamage(ACharacter & car1, ACharacter & car2)
{
	car2.setArmor(car1.getDamage());
	car1.setArmor(car2.getDamage());
}
