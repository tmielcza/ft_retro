// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   EntityManager.cpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 17:01:10 by rduclos           #+#    #+#             //
//   Updated: 2015/01/19 15:52:54 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "EntityManager.hpp"
#include "Enemy.hpp"
#include "Player.hpp"

EntityManager::EntityManager(void)
{
	this->_enemy = new Enemy*[MAX_ENEMY];
	this->_missile = new Missile*[MAX_MISSILE];
	this->_player = new Player(20, 20, 4, 1);
	for (int i = 0; i < MAX_ENEMY; i++)
		this->_enemy[i] = NULL;
	for (int i = 0; i < MAX_MISSILE; i++)
		this->_missile[i] = NULL;
	this->_enemyid = 0;
	this->_missileid = 0;
	this->_nbenemy = 0;
	this->_nbmissile = 0;
}

EntityManager::EntityManager(EntityManager const & copy)
{
	*this = copy;
}


EntityManager::~EntityManager(void)
{
	delete this->_enemy;
	delete this->_missile;
	delete this->_player;
}

EntityManager &		EntityManager::operator=(EntityManager const & ass)
{
	this->_enemy = ass._enemy;
	this->_player = ass._player;
	this->_nbenemy = ass._nbenemy;
	this->_enemyid = ass._enemyid;
	this->_missile = ass._missile;
	this->_missileid = ass._missileid;
	this->_nbmissile = ass._nbmissile;
	return (*this);
}

Missile &       EntityManager::addMissile(void)
{
	Missile*		miss = new Missile();

	if (this->_missile[this->_nbmissile] != NULL)
	{
		delete this->_missile[this->_nbmissile];
		this->_missile[this->_nbmissile] = NULL;
	}
	this->_missile[this->_nbmissile] = miss;
	this->_nbmissile = (this->_nbmissile + 1) % MAX_MISSILE;
	return (*miss);
}

Enemy &         EntityManager::popEnemy(e_shape shape, int size)
{
	Enemy*		enemy = new Enemy(shape, size);

	this->_enemy[this->_enemyid] = enemy;
	this->_nbenemy++;
	this->_enemyid = (this->_enemyid + 1) % MAX_ENEMY;
	return (*enemy);
}

void			EntityManager::delMissile(Missile** miss)
{
	delete *miss;
	*miss = NULL;
	this->_nbmissile--;
}

void			EntityManager::delEnemy(Enemy** enemy)
{
	delete *enemy;
	*enemy = NULL;
	this->_nbenemy--;	
}

bool			EntityManager::collide(const VectorUnit& u1, const VectorUnit& u2) const
{
	for (int i = 0; i < u1.getLineNb(); i++)
	{
		Line	u1Pos(u1.getX(), u1.getY(), u1.getX(), u1.getY());
		for (int j = 0; j < u2.getLineNb(); j++)
		{
			Line	u2Pos(u2.getX(), u2.getY(), u2.getX(), u2.getY());
			if (Line::lineIntersect(u1.getLines()[i] + u1Pos, u2.getLines()[j] + u2Pos))
			{
				return (true);
			}
		}
	}
	return (false);
}

void		EntityManager::moveEverything(float time)
{
	int		i;

	i = 0;
	while (i < MAX_MISSILE || i < MAX_ENEMY)
	{
		if (i < MAX_MISSILE && this->_missile[i] != NULL)
		{
			this->_missile[i]->move(time);
			if (this->_missile[i]->getX() < -10)
				this->delMissile(this->_missile + i);
		}
		if (i < MAX_ENEMY && this->_enemy[i] != NULL)
		{
			this->_enemy[i]->move(time);
			if (this->_enemy[i]->getX() < -10)
				this->delEnemy(this->_enemy + i);
		}
		i++;
	}
}

void		EntityManager::drawEverything(Screen& screen)
{
	int		i;

	i = 0;
	while (i < MAX_MISSILE || i < MAX_ENEMY)
	{
		if (i < MAX_MISSILE && this->_missile[i] != NULL)
			screen.drawThing(*this->_missile[i]);
		if (i < MAX_ENEMY && this->_enemy[i] != NULL)
			screen.drawThing(*this->_enemy[i]);
		i++;
	}
	screen.drawThing(*this->_player);
	screen.draw();
}

void		EntityManager::collideEverything()
{
	for (int i = 0; i < MAX_ENEMY; i++)
	{
		if (this->_enemy[i] != NULL)
		{
			if (collide(*this->_player, *this->_enemy[i]))
				exit(0);
			else
			{
				for (int j = 0; j < MAX_MISSILE; j++)
				{
					if (this->_missile[j] != NULL
						&& collide(*this->_missile[j], *this->_enemy[i]))
					{
						this->_player->Score(this->_player->Score() + 1);
						ACharacter::colDamage(*this->_missile[j], *this->_enemy[i]);
						if (this->_missile[j]->getArmor() == 0)
							this->delMissile(this->_missile + j);
						if (this->_enemy[i]->getArmor() == 0)
							this->delEnemy(this->_enemy + i);
						break;
					}
				}
			}
		}
	}
}

void		EntityManager::setPlayerRotSpeed(float rot, float speed)
{
	this->_player->setSpeed(speed);
	this->_player->setRot(rot);
}

void		EntityManager::setPlayerRotate(float time)
{
	this->_player->rotate(time);
}

Player &		EntityManager::getPlayer(void)
{
	return(*this->_player);
}

void			EntityManager::popWave(void)
{
	static int			j;
	static int			k;

	if (this->_nbenemy < 5 + this->_player->Score() / 10)
	{
		for (int i = 0; i < this->_player->Score() / 30 + 1; i++)
		{
			if (j % 2 == 1)
			{
				k = 1 + rand() % 4;
				Enemy& enemy = this->popEnemy(__triangle, k);
				float y = 5 + rand() % static_cast<int>(SCR_FRAG_WIDTH / SCR_RATIO);
				enemy.setPos(100 + j * 10, y);
				enemy.setMove(-1, 0);
				enemy.setSpeed(20.f);
				enemy.rotateAll(((float)(rand() % 1000) / 500 - 1) * 0.1 + (float)(y - 35) / 200);
			}
			else
			{
				k = 1 + rand() % 7;
				Enemy& enemy = this->popEnemy(__square, k);
				enemy.setPos(130 + j * 10, 5 + rand() % 70);
				enemy.setMove(-1, 0);
				enemy.setSpeed((float)(40.f / k));
			}
			j = (j + 1) % 20;
		}
	}
}
