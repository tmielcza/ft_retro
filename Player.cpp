// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Player.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 00:39:33 by rduclos           #+#    #+#             //
//   Updated: 2015/01/16 14:45:00 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <cstdlib>
#include "Player.hpp"
#include "Missile.hpp"
#include "EntityManager.hpp"

Player::Player(float x, float y, int armormax, int damage) :
	VectorUnit(__square, 2)
{
	this->_x = x;
	this->_y = y;
	this->_armormax = armormax;
	this->_damage = damage;
	this->_type = "Player";
	this->_armor = armormax;
	this->_score = 0;
}

Player::Player(Player const & copy)
{
	*this = copy;
}

Player::Player(void) :
	VectorUnit(__square, 1)
{
	this->_x = 20;
	this->_y = 20;
	this->_armormax = 4;
	this->_damage = 1;
	this->_type = "Player";
	this->_armor = this->_armormax;
	this->_score = 0;
}

Player&		Player::operator=(const Player& rhs)
{
	this->_x = rhs._x;
	this->_y = rhs._y;
	this->_armormax = rhs._armormax;
	this->_damage = rhs._damage;
	this->_type = rhs._type;
	this->_armor = rhs._armor;
	return (*this);
}

void				Player::shoot(EntityManager& entity)
{
	float ypos = (float)(rand() % 10) / 5.f - 1;
	Missile& miss = entity.addMissile();
	Missile& miss2 = entity.addMissile();
	Missile& miss3 = entity.addMissile();

	miss.setPos(this->_x, this->_y + ypos);
	miss2.setPos(this->_x, this->_y - 1 + ypos);
	miss3.setPos(this->_x, this->_y + 1 + ypos);
	miss.setSpeed(50.f);
	miss.setMove(1.f, 0);
	miss2.setSpeed(50.f);
	miss2.setMove(1.f, 0);
	miss3.setSpeed(50.f);
	miss3.setMove(1.f, 0);
	miss2.rotateAll(0.2f);
	miss3.rotateAll(-0.2f);
}

Player::~Player(void)
{
}

void		Player::makeMove(float time, float vecx, float vecy)
{
	float	coef = time * this->_speed;

	if ((vecx >= 0 && this->_x < SCR_FRAG_WIDTH) || (vecx < 0 && this->_x > 4))
		this->_x += (vecx * coef);
	if ((vecy >= 0 && this->_y < SCR_FRAG_WIDTH / SCR_RATIO) || (vecy < 0 && this->_y > 4))
		this->_y += (vecy * coef);
}

void				Player::die(void)
{
}

int					Player::Score(void) const
{
	return (this->_score);
}

void				Player::Score(int score)
{
	this->_score = score;
}
