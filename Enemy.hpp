// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Enemy.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 20:34:13 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 17:31:03 by rduclos          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENEMY_HPP
# define ENEMY_HPP

#include "ACharacter.hpp"
#include "VectorUnit.hpp"

class Enemy : public ACharacter, public VectorUnit
{

	public:

	Enemy(e_shape shape, int size);
	Enemy(Enemy const & copy);
	Enemy(void);
	~Enemy(void);

	Enemy&			operator=(Enemy const & rhs);

	void			die(void);
};

#endif
