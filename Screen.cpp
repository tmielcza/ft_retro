// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Screen.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 17:19:26 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/19 16:18:41 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <ncurses.h>
#include <cstdlib>
#include <algorithm>
#include "Screen.hpp"

#include <iostream>

Screen	Screen::_instance = Screen(300, 100);

Screen&	Screen::Instance(void)
{
	return (_instance);
}

Screen::Screen(void)
{
}

Screen::Screen(int x, int y) : _x(x), _y(y)
{
}

Screen::~Screen(void)
{
	this->dispose();
}

Screen::Screen(const Screen& src)
{
	*this = src;
}

Screen&		Screen::operator=(const Screen& rhs)
{
	this->_win = rhs._win;
	this->_x = rhs._x;
	this->_y = rhs._y;
	return (*this);
}

void		Screen::init(void)
{
	initscr();
	cbreak();
	noecho();
	mousemask(REPORT_MOUSE_POSITION, NULL);
	curs_set(0);
	this->_win = newwin(this->_y, this->_x, 0, 0);
	intrflush(this->_win, false);
	nodelay(this->_win, true);
	keypad(this->_win, true);
	timeout(-1);
	this->_buf = new std::string(this->_x * this->_y, ' ');
}

void		Screen::dispose(void)
{
	endwin();
}

void		Screen::drawThing(IDisplayable& thing) const
{
	thing.display(*this);
}

void		Screen::displayDeath(void) const
{
	this->drawLine(Line(20, 20, 30, 30));
	this->drawLine(Line(30, 30, 20, 40));
	this->drawLine(Line(20, 40, 20, 20));

	this->drawLine(Line(50, 20, 40, 30));
	this->drawLine(Line(40, 30, 50, 40));
	this->drawLine(Line(40, 30, 50, 30));

	this->drawLine(Line(65, 20, 60, 40));
	this->drawLine(Line(65, 20, 70, 40));
	this->drawLine(Line(62, 30, 68, 30));

	this->drawLine(Line(80, 20, 90, 30));
	this->drawLine(Line(90, 30, 80, 40));
	this->drawLine(Line(80, 40, 80, 20));
}

void		Screen::drawText(const std::string txt, int x, int y) const
{
	mvwprintw(this->_win, y, x, txt.c_str());
}

void		Screen::drawPix(int x, int y) const
{
	if (x >= 0 && y >= 0 && x < this->_x && y < this->_y)
	{
		this->_buf->at(x + this->_x * y) = '0' + rand() % ('~' - '0');
	}
}

void		Screen::drawLine(const Line& line) const
{
	float coef = this->_x / (SCR_FRAG_WIDTH * 2.5);
	int dx,dy,i,xinc,yinc,cumul,x,y ;

	x = line.xa() * 2.5 * coef;
	y = line.ya() * coef;
	dx = (line.xb() - line.xa()) * 2.5 * coef;
	dy = (line.yb() - line.ya()) * coef;
	xinc = ( dx > 0 ) ? 1 : -1 ;
	yinc = ( dy > 0 ) ? 1 : -1 ;
	dx = std::abs(dx) ;
	dy = std::abs(dy) ;
	this->drawPix(x, y);
	if ( dx > dy ) {
		cumul = dx / 2 ;
		for ( i = 1 ; i <= dx ; i++ )
		{
			x += xinc ;
			cumul += dy ;
			if ( cumul >= dx )
			{
				cumul -= dx ;
				y += yinc ;
			}
			this->drawPix(x, y);
		}
	}
	else
	{
		cumul = dy / 2 ;
		for ( i = 1 ; i <= dy ; i++ )
		{
			y += yinc ;
			cumul += dx ;
			if ( cumul >= dy )
			{
				cumul -= dy ;
				x += xinc ;
			}
			this->drawPix(x, y);
		}
	}
}

void		Screen::draw(void)
{
	mvwprintw(this->_win, 0, 0, this->_buf->c_str());
}

void		Screen::refresh(void)
{
	wrefresh(this->_win);
}

void		Screen::clear(void)
{
	werase(this->_win);
}

int			Screen::getChar(void)
{
	return (wgetch(this->_win));
}

void		Screen::autoResize(void)
{
//	int		lines = LINES;
	int		columns = COLS;

	int		scrw = columns;
	int		scrh = scrw / SCR_RATIO;
//	int		scrx = 0;
//	int		scry = lines / 2 - scrh / 2;

	this->_x = scrw;
	this->_y = scrh;

	this->_buf->resize(scrw * scrh);
	std::fill(this->_buf->begin(), this->_buf->end(), ' ');

	wresize(this->_win, scrh, scrw);
}
