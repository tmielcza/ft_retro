// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   IDisplayable.hpp                                   :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 14:38:20 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/10 19:35:07 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef IDISPLAYABLE
# define IDISPLAYABLE

//# include "Screen.hpp"

class Screen;
class IDisplayable
{
public:
	virtual ~IDisplayable() {}
	virtual void	display(const Screen& src) const = 0;
};

#endif // IDISPLAYABLE
