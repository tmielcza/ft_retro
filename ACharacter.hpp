// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   ACharacter.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 20:15:04 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 16:03:41 by rduclos          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ACHARACTER_HPP
# define ACHARACTER_HPP

#include <iostream>
#include <string>

class ACharacter
{
	public:

	ACharacter(std::string type, int armormax, int damage);
	ACharacter(ACharacter const & copy);
	ACharacter(void);
	~ACharacter(void);
	ACharacter	&	operator=(ACharacter const & ass);
	void			die(void) const;
	int				getArmor(void) const;
	std::string		getType(void) const;
	int				getDamage(void) const;
	void			setArmor(int damage);
	static void		colDamage(ACharacter & car1, ACharacter & car2);

	protected:

	std::string		_type;
	int				_armor;
	int				_armormax;
	int				_damage;

};

#endif
