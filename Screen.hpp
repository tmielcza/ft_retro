// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Screen.hpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 12:27:30 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/19 15:33:45 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef SCREEN_HPP
# define SCREEN_HPP

# include <ncurses.h>
# include <iostream>
# include "Line.hpp"
# include "IDisplayable.hpp"

# define SCR_FRAG_WIDTH (100)
# define SCR_RATIO (16.f / 9.f)

class Screen
{
private:
	Screen(void);

	void			drawPix(int x, int y) const;

	static Screen	_instance;

	std::string		*_buf;

public:
	Screen(int x, int y);
	Screen(const Screen&);
	~Screen(void);

	Screen&	operator=(const Screen&);

	void			drawThing(IDisplayable& thing) const;
	void			init(void);
	void			dispose(void);

	void			refresh(void);
	void			clear(void);
	int				getChar(void);

	void			draw(void);
	void			drawLine(const Line&) const;
	void			drawText(const std::string txt, int x, int y) const;

	void			displayDeath(void) const;

	void			autoResize(void);

	static Screen&	Instance(void);

protected:
	int				_x;
	int				_y;
	WINDOW*			_win;

};

#endif // SCREEN_HPP
