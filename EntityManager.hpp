// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   EntityManager.hpp                                  :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 20:15:04 by rduclos           #+#    #+#             //
//   Updated: 2015/01/19 16:07:14 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef ENTITYMANAGER_HPP
# define ENTITYMANAGER_HPP

#include <iostream>
#include <string>
#include "Missile.hpp"
#include "ACharacter.hpp"
#include "Enemy.hpp"
#include "Player.hpp"
#include "Line.hpp"

#define MAX_ENEMY 400
#define MAX_MISSILE 300

class EntityManager
{
	public:

	EntityManager(EntityManager const & copy);
	EntityManager(void);
	~EntityManager(void);
	EntityManager	&	operator=(EntityManager const & ass);

	Missile &		addMissile(void);
	Enemy &			popEnemy(e_shape shape, int size);
	void			delMissile(Missile** miss);
	void			delEnemy(Enemy** enemy);

	void			popWave(void);
	void			moveEverything(float time);
	void			drawEverything(Screen& screen);
	void			collideEverything(void);
	void			setPlayerRotSpeed(float rot, float speed);
	void			setPlayerRotate(float time);
	Player	&		getPlayer(void);

	protected:

	Player			*_player;
	Enemy			**_enemy;
	int				_nbenemy;
	int				_enemyid;
	Missile			**_missile;
	int				_missileid;
	int				_nbmissile;

	bool			collide(const VectorUnit& u1, const VectorUnit& u2) const;
};

#endif
