// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   VectorUnit.hpp                                     :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 01:01:40 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/14 16:13:16 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef VECTORUNIT_HPP
# define VECTORUNIT_HPP

# include "IDisplayable.hpp"
# include "IMovable.hpp"
# include "Line.hpp"
# include "Screen.hpp"

enum e_shape {__square, __triangle, __line};

class VectorUnit : public IDisplayable, public IMovable
{
private:
//	Line*		UnitShape(e_shape shape, int size);
	Line*		UnitSquare(int size);
	Line*		UnitTriangle(int size);
	Line*		UnitLine(int size);

public:
	VectorUnit(void);	
	VectorUnit(const VectorUnit&);
	VectorUnit(e_shape shape, int size);
	VectorUnit(Line lines[], const int nb);
	~VectorUnit(void);

	VectorUnit&	operator=(const VectorUnit&);

	void		display(const Screen& screen) const;

	void		setPos(float x, float y);

	void		setMove(float vecx, float vecy);
	void		setSpeed(float speed);
	void		move(float time);
	void		makeMove(float time, float vecx, float vecy);

	void		setRot(float rs);
	void		rotate(float time);
	void		makeRotate(float rot);
	void		rotateAll(float rot);

	void		multSize(float s);

	Line		*getLines(void) const;
	int			getLineNb(void) const;
	int			getX(void) const;
	int			getY(void) const;

protected:
	Line*		_lines;
	int			_nblines;

	float		_vecx;
	float		_vecy;
	float		_speed;
	float		_rot;
	float		_rotspeed;

	float		_x;
	float		_y;
};

#endif // VECTORUNIT_HPP
