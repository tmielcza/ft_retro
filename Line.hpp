// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Line.hpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 17:00:46 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/14 16:12:16 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef LINE_HPP
# define LINE_HPP

# include "IDisplayable.hpp"

//class IDisplayable;
class Line : public IDisplayable
{
public:
	Line(void);	
	Line(float xa, float xb, float ya, float yb);
	Line(int xa, int xb, int ya, int yb);
	Line(const Line&);
	~Line(void);

	Line&		operator=(const Line&);
	const Line	operator+(const Line&) const;
	const Line	operator-(const Line&) const;
	const Line	operator*(const float rhs) const;

	void	display(const Screen& screen) const;

	float		xa(void) const;
	float		ya(void) const;
	float		xb(void) const;
	float		yb(void) const;

/*
	int		xa(void) const;
	int		ya(void) const;
	int		xb(void) const;
	int		yb(void) const;
*/
	static bool	lineIntersect(const Line& l1, const Line& l2);

protected:
	float		_xa;
	float		_xb;
	float		_ya;
	float		_yb;
};

#endif // LINE_HPP
