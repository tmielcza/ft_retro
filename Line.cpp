// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Line.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 19:40:12 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/14 19:15:11 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <iostream>
#include "Line.hpp"

Line::Line(void)
{
}

Line::Line(int xa, int ya, int xb, int yb)
{
	this->_xa = xa;
	this->_ya = ya;
	this->_xb = xb;
	this->_yb = yb;
}

Line::Line(float xa, float ya, float xb, float yb)
{
	this->_xa = xa;
	this->_ya = ya;
	this->_xb = xb;
	this->_yb = yb;
}

Line::Line(const Line& src)
{
	*this = src;
}

Line::~Line(void)
{
}

Line&	Line::operator=(const Line& rhs)
{
	this->_xa = rhs._xa;
	this->_ya = rhs._ya;
	this->_xb = rhs._xb;
	this->_yb = rhs._yb;
	return (*this);
}

const Line	Line::operator+(const Line& rhs) const
{
	Line line(*this);

	line._xa += rhs._xa;
	line._ya += rhs._ya;
	line._xb += rhs._xb;
	line._yb += rhs._yb;
	return (line);
}

const Line	Line::operator-(const Line& rhs) const
{
	Line line(*this);

	line._xa -= rhs._xa;
	line._ya -= rhs._ya;
	line._xb -= rhs._xb;
	line._yb -= rhs._yb;
	return (line);
}

const Line	Line::operator*(const float rhs) const
{
	Line line(*this);

	line._xa *= rhs;
	line._ya *= rhs;
	line._xb *= rhs;
	line._yb *= rhs;

	return (line);
}

void	Line::display(const Screen& screen) const
{
	(void)screen;
}

float	Line::xa(void) const
{
	return (this->_xa);
}

float	Line::ya(void) const
{
	return (this->_ya);
}

float	Line::xb(void) const
{
	return (this->_xb);
}

float	Line::yb(void) const
{
	return (this->_yb);
}

bool	Line::lineIntersect(const Line& l1, const Line& l2)
{
	float s1_x, s1_y, s2_x, s2_y;
	s1_x = l1.xb() - l1.xa();
	s1_y = l1.yb() - l1.ya();
	s2_x = l2.xb() - l2.xa();
	s2_y = l2.yb() - l2.ya();

	float s, t;
	s = (-s1_y * (l1.xa() - l2.xa()) + s1_x * (l1.ya() - l2.ya())) / (-s2_x * s1_y + s1_x * s2_y);
	t = ( s2_x * (l1.ya() - l2.ya()) - s2_y * (l1.xa() - l2.xa())) / (-s2_x * s1_y + s1_x * s2_y);

	if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
		return true;
	else
		return false;
}
