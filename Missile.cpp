// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Missile.cpp                                         :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/11 00:39:33 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 22:20:55 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include "Missile.hpp"

Missile::Missile(float x, float y, int armormax, int damage) :
	VectorUnit(__line, 3)
{
	this->_x = x;
	this->_y = y;
	this->_armormax = armormax;
	this->_damage = damage;
	this->_type = "Missile";
	this->_armor = armormax;
	this->_speed = 1.f;
}

Missile::Missile(Missile const & copy)
{
	*this = copy;
}

Missile::Missile(void) :
	VectorUnit(__line, 3)
{
	this->_x = 0;
	this->_y = 0;
	this->_armormax = 1;
	this->_damage = 1;
	this->_type = "Missile";
	this->_armor = this->_armormax;
	this->_speed = 1.f;
}

Missile&		Missile::operator=(const Missile& rhs)
{
	this->_x = rhs._x;
	this->_y = rhs._y;
	this->_armormax = rhs._armormax;
	this->_damage = rhs._damage;
	this->_type = rhs._type;
	this->_armor = rhs._armor;
	this->_speed = rhs._speed;
	return (*this);
}

Missile::~Missile(void)
{

}

void				Missile::die(void)
{
	
}
