// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   main.cpp                                           :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 23:03:21 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 12:16:05 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <ncurses.h>
#include <iostream>
#include <cmath>
#include "Screen.hpp"
#include "VectorUnit.hpp"
#include "Line.hpp"

#include <unistd.h>

int main(void)
{
	Screen screen(150, 80);
	Line lines[4] = {Line(-5, -5, -5, 5),
					 Line(-5, 5, 5, 5),
					 Line(5, 5, 5, -5),
					 Line(5, -5, -5, -5)};
	VectorUnit unit(lines, 4);
	VectorUnit unit2(lines, 4);
	unit.setPos(10, 10);
	unit2.setPos(20, 20);
	screen.init();

	screen.drawThing(unit);

	Line line(0, 0, 20, 20);

	screen.drawThing(line);

	std::cout << "mdr";

	screen.refresh();

	std::cout << "mdr";

	unit.setRot(0.0003);
	unit2.setRot(0.0005);

	screen.drawThing(unit);

//	sleep(3);

	while (true)
	{
		screen.clear();
		screen.drawThing(unit);
		screen.drawThing(unit2);
		unit.move();
		unit2.move();
		screen.refresh();
//		sleep(1);
	}

/*
	float r = 0;
	int xi = -3;
	int yi = -3;
	int xf = 3;
	int yf = 3;

	while (true)
	{
		screen.clear();
		int xi2 = (float)xi * std::cos(r) - (float)yi * std::sin(r) + 5;
		int yi2 = (float)xi * std::sin(r) + (float)yi * std::cos(r) + 5;
		int xf2 = (float)xf * std::cos(r) - (float)yf * std::sin(r) + 5;
		int yf2 = (float)xf * std::sin(r) + (float)yf * std::cos(r) + 5;

		Line test(xi2, yi2, xf2, yf2);

		screen.drawLine(test);
		r += 0.0003;
		std::cout << "LOL";
		screen.refresh();
	}

*/
}
