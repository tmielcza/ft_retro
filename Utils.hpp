// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Utils.hpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/16 11:57:01 by tmielcza          #+#    #+#             //
//   Updated: 2015/01/16 11:57:55 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef UTILS_HPP
# define UTILS_HPP

namespace Utils
{
	double		deltaTime(void);
	double		ft_usec_time(void);
}

#endif // UTILS_HPP
