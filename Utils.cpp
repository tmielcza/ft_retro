// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Utils.cpp                                          :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: tmielcza <tmielcza@student.42.fr>          +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/16 11:52:03 by tmielcza          #+#    #+#             //
//   Updated: 2015/04/02 17:49:00 by tmielcza         ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#include <sys/time.h>
#include <stdlib.h>
#include <time.h>

namespace Utils
{

	double			ft_usec_time(void)
	{
		struct timeval		tv;
		struct timezone		tz;
		struct tm			*tm;
		double				my_time;

		gettimeofday(&tv, &tz);
		tm = localtime(&tv.tv_sec);
		my_time = 0;
		my_time = (my_time + tm->tm_hour) * 60;
		my_time = (my_time + tm->tm_min) * 60;
		my_time = ((my_time + tm->tm_sec) * 1000000) + tv.tv_usec;
		return (my_time);
	}

	double		deltaTime(void)
	{
		double					time;
		double					tmp;
		static double			last;

		time = ft_usec_time();
		if (last != 0)
		{
			tmp = time - last;
		}
		else
			tmp = last;
		last = time;
		return (tmp / 1000000.f);
	}

}
