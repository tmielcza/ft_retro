// ************************************************************************** //
//                                                                            //
//                                                        :::      ::::::::   //
//   Missile.hpp                                        :+:      :+:    :+:   //
//                                                    +:+ +:+         +:+     //
//   By: rduclos <rduclos@student.42.fr>            +#+  +:+       +#+        //
//                                                +#+#+#+#+#+   +#+           //
//   Created: 2015/01/10 20:34:13 by rduclos           #+#    #+#             //
//   Updated: 2015/01/11 16:44:46 by rduclos          ###   ########.fr       //
//                                                                            //
// ************************************************************************** //

#ifndef Missile_HPP
# define Missile_HPP

#include "ACharacter.hpp"
#include "VectorUnit.hpp"

class Missile : public ACharacter, public VectorUnit
{

	public:

	Missile(float x, float y, int armor, int damage);
	Missile(Missile const & copy);
	Missile(void);
	~Missile(void);

	Missile&			operator=(Missile const & rhs);
	void			die(void);
};

#endif
